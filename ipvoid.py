# Author: Jay Park
# Usage : 
#      1. Dump IP addresses into ips.txt
#      2. Save the below code as ipvoid.py in the same directory
#      3. Run as python ipvoid.py ips.txt
#      4. ipvoidResult.csv will be generated in the directory

import requests
import os,sys
from bs4 import BeautifulSoup
import pandas as pd

with open(sys.argv[1],'r') as f:
    ips = [l.strip() for l in f]
 
#ips = ['32.5.5.5', '94.200.234.6', '172.16.0.1']
list_blacklists = []
list_ips = []
list_safelists = []
list_asns = []
list_countries =[]
list_cities = []

url = "https://www.ipvoid.com/ip-blacklist-check/"
 
for ip in ips:
    payload = "ip=%s" %ip
    headers = {
        'Content-Type': "application/x-www-form-urlencoded",
        'cache-control': "no-cache"
        }
 
    response = requests.request("POST", url, data=payload, headers=headers)
    html = response.text
    soup = BeautifulSoup(html,'html.parser')
    try:
        blacklist = soup.find("td", text="Blacklist Status").find_next_sibling("td").text
        asn = soup.find("td", text="ASN Owner").find_next_sibling("td").text
        country = soup.find("td", text="Country Code").find_next_sibling("td").text
        city = soup.find("td", text="City").find_next_sibling("td").text
        
    except:
        asn = 'Invalid'
        country = 'Invalid'
        city = 'Invalid'

    list_ips.append(ip)
    list_blacklists.append(blacklist)
    list_asns.append(asn)
    list_countries.append(country)
    list_cities.append(city)
    
df = pd.DataFrame([list_ips, list_blacklists, list_asns, list_countries, list_cities]).T
df.columns=['IP Address', 'Blacklist Status', 'ASN Owner', 'Country', 'City']
df.to_csv('ipvoidResult.csv')

