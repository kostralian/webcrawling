# Author: Jay Park
# Usage:
#    1. Dump fortigate event IDs into a text file such as winSecEventIDs.txt
#    2. Save the below code into winSecEvent.py
#    3. Run as python winSecEvent.py winSecEventIDs.txt
 
import requests
import os,sys
from bs4 import BeautifulSoup
import re
import pandas as pd
 
with open(sys.argv[1]) as f:
    eventIDs = [l.strip() for l in f]
 
#eventIDs = ['win_sec4457', 'win_sec4776']
list_events = []
list_descriptions = []
 
url = "https://www.ultimatewindowssecurity.com/securitylog/encyclopedia/event.aspx?eventid="
 
for eventID in eventIDs:
    num = str(re.search(r'\d+', eventID).group())
    req = requests.get(url + num)
    html = req.text
    soup = BeautifulSoup(html, 'html.parser')
    try:
        desc = soup.find('p',{"class": "hey"})
        description = desc.text.strip()
        
    except:
        description = 'Not Found'
    
    list_events.append(eventID)
    list_descriptions.append(description)

df = pd.DataFrame([list_events, list_descriptions]).T
df.columns=['Event_ID', 'Description']
df.to_csv('winSecEventIDs.csv')
