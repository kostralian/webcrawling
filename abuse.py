# Author: Jay Park
# Usage :
#      1. Dump IP addresses into ips.txt
#      2. Save the below code as abuse.py in the same directory
#      3. Run as python abuse.py ips.txt
#      4. abused.csv file will be generated
  
import requests
import os,sys
from bs4 import BeautifulSoup
import pandas as pd
import time
   
with open(sys.argv[1],'r') as f:
    ips = [l.strip() for l in f]
    
ips = ['107.173.145.173','94.200.234.6','172.16.0.1']
#'94.200.234.6', '172.16.0.1'
   
list_ips = []
list_descriptions1 = []
list_descriptions2 = []
list_isps = []
list_usages = []
list_hosts =[]
list_domains = []
list_countries = []
list_cities = []
   
url = "https://www.abuseipdb.com/check/"
  
for ip in ips:
    num = str(ip)
    req  = requests.get(url + num)
    html = req.text
    #print(html)
    soup = BeautifulSoup(html, 'html.parser')
  
    try:
        div = soup.find('div',{"class": "well"})
        description1 = div.h3.text.strip()
        description2 = div.p.text.strip()
        isp = soup.find("th", text="ISP").find_next_sibling("td").text.strip()
        usage = soup.find("th", text="Usage Type").find_next_sibling("td").text.strip()
        #host = soup.find("th", text="Hostname(s)").find_next_sibling("td").text.strip()
        domain = soup.find("th", text="Domain Name").find_next_sibling("td").text.strip()
        country = soup.find("th", text="Country").find_next_sibling("td").text.strip()
        city = soup.find("th", text="City").find_next_sibling("td").text.strip()
    except:
        isp = 'Invalid'
        usage = 'Invalid'
        # host = 'Invalid'
        domain = 'Invalid'
        country = 'Invalid'
        city = 'Invalid'
                         
    list_ips.append(ip)
    list_descriptions1.append(description1)
    list_descriptions2.append(description2)
    list_isps.append(isp)
    list_usages.append(usage)
    #list_hosts.append(host)
    list_domains.append(domain)
    list_countries.append(country)
    list_cities.append(city)
       
df = pd.DataFrame([list_ips, list_descriptions1, list_descriptions2, list_isps, list_usages, list_domains, list_countries, list_cities]).T
df.columns=['IP Address', 'Description1', 'Description2', 'ISP', 'Usage', 'Domain Name', 'Country', 'City']
df.to_csv('abused.csv')
# print(df)