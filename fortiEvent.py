# Author: Jay Park
# Usage : 1. Dump fortigate event IDs into a text file such as fortiEventIDs.txt
#         2. Save the below code as fortiEvent.py
#         3. Run as python fortigateEvent.py fortiEventIDs.txt
 
import requests
import os,sys
from bs4 import BeautifulSoup
import re
import pandas as pd
 
with open(sys.argv[1]) as f:
    eventIDs = [l.strip() for l in f]
 
list_events = []
list_descriptions = []
 
url = "https://fortiguard.com/encyclopedia/ips/"
 
for eventID in eventIDs:
    num = str(re.search(r'\d+', eventID).group())
    req  = requests.get(url + num)
    html = req.text
    soup = BeautifulSoup(html, 'html.parser')
    try:
        div = soup.findAll('div',{"class": "detail-item"})[1]
        desc = div.text
        description = desc.replace('Description','').strip()
        
    except:
        description='Not Found'
        
    list_events.append(eventID)
    list_descriptions.append(description)
 
df = pd.DataFrame([list_events, list_descriptions]).T
df.columns=['Event_ID', 'Description']
df.to_csv('fortiEventsAll.csv')