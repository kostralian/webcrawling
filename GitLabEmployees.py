# Author: Jay Park
# This file will generate gitlabEmployees.csv in the working
# directory with all Gitlab employee names, roles and locations 

import requests
from bs4 import BeautifulSoup
import pandas as pd 

req = requests.get("https://about.gitlab.com/company/team/")
html = req.text
soup = BeautifulSoup(html, 'html.parser')

employees = soup.find_all('div',{"class": "front"})
names =[]
roles = []
locations = []
for employee in employees:
    name=employee.h3.text
    role=employee.h4.text
    if employee.find("h5") is not None:
        location=employee.h5.a.text
    else:
        location='NA'
    #print(name +"|" + role + "|" + location)
    names.append(name)
    roles.append(role)
    locations.append(location)

df = pd.DataFrame([names, roles, locations]).T
df.columns = ['name', 'role', 'location']
df.to_csv('gitlabEmployees.csv')