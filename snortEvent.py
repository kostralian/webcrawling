# Author: Jay Park
# Usage : 1. Dump snort event IDs into a text file such as snortEventIDs.txt
#         2. Save the below code as snortEvent.py
#         3. Run as python snortEvent.py snortEventIDs.txt
  
import requests
from bs4 import BeautifulSoup
import pandas as pd
  
with open(sys.argv[1]) as f:
    eventIDs = [l.strip() for l in f]
  
#eventIDs=['snort-1:34226', 'snort-1:34227']
list_events = []
list_descriptions = []
  
url = "https://www.snort.org/rule_docs/"
  
for eventID in eventIDs:
    num = eventID.replace('snort-','').replace(':','-').strip()
    req  = requests.get(url + num)
    html = req.text
    soup = BeautifulSoup(html, 'html.parser')
    try:
        div = soup.find('div',{"class": "col-sm-10 col-sm-offset-1 rule_listing"})
        description = div.p.text
        
    except:
        decription = 'Not Found'
            
    list_events.append(eventID)
    list_descriptions.append(description)
  
df = pd.DataFrame([list_events, list_descriptions]).T
df.columns=['Event_ID', 'Description']
df.to_csv('snortEventsAll.csv')